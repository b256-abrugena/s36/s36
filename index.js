/*
	TOPIC 20230331: Separation of Concerns

	WHY DO WE HAVE TO SEPARATE CERTAIN CODES?
	1. Better code readability
	2. Improved stability
	3. Better code maintainability

	WHAT DO WE SEPARATE?
	1. Models - contains what objects are needed in our API (users, courses)
			Object schemas and relationships are defined in models.
			Serves also as the blueprint.

			Example:

			const courseSchema = new mongoose.Schema({
				name: {...
				},
				description: {...
				},
				price: {...
				},
				isActive: {...
				},
				createdOn: {...
				},
				enrollees: {...
				}
			})

	2. Controllers
		- Contains instructions on HOW your API will perform its intended tasks
		- Kung paano gagana ang API mo?
		- Mongoose model queries are used here, examples are:
			- Model.find()
			- Model.findOne()
			- Model.findByIdAndDelete()
			- Model.findByIdAndUpdate()
		Example:
			module.exports.getAll = () ==> {
				return Course.find({ isActive: true }).then(courses => courses)
			}

	3. Routes
		- Defines when particular controllers will be used
		- A specific controller action will be called when the specific HTTP method is received on a specific API endpoint.
		- Responsible on the URL part.
		- Kung kailan gagana ang API?

	We separate our concerns through the use of JS modules.
	JS Modules are modules that are self-contained units of functionality that can be shared and reused.
		Examples of modules: EXPRESS, HTTP, MONGOOSE
		Etong mga modules ay shineshare through the use of "require".

*/


// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoutes.js")

// Server setup
const app = express(); // "express()" is the Express Method
const port = 3001;

// MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin1234@halmontedb.4urphkk.mongodb.net/B256_to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`Were connected to the cloud database`));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}))

// Add the task route
// Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route
// "/task" as the parent route
app.use("/task", taskRoute)

// Server Listening
app.listen(port, () => console.log(`Server is running at port ${port}`));

